---
layout: page
title: Academic Elements
description: Integer vehicula pretium nunc, ut commodo lacus vestibulum id. Etiam fermentum neque arcu, gravida fermentum purus blandit ac. 
permalink: research/
redirect_from:
  - interests/
  - profile/
---

<a name="top"></a>
Integer vehicula pretium nunc, ut commodo lacus vestibulum id. Etiam fermentum neque arcu, gravida fermentum purus blandit ac. Nunc enim sapien, ullamcorper ut lectus eu, viverra feugiat leo. Duis elementum at leo sit amet pulvinar. 

<hr style="margin: 10px 0 10px 0; padding: 0;">
<ul style="margin: 0 0 0 0; padding: 0; align-items: center; vertical-align: middle;" class="icons" >
    <li><a style="font-size: 10px;" class="icon fa-angle-double-right"></a>&nbsp;View Profile Externally </li>
      <li><a href=" {{ site.author.linkedin }} " target="_blank"  rel="noopener noreferrer" ><span class="label">LinkedIn</span></a></li>
      <li><a href=" {{ site.author.ads }} " target="_blank"  rel="noopener noreferrer" ><span class="label">NASA ADS</span></a></li>
      <li><a href=" {{ site.author.orcid }} " target="_blank"  rel="noopener noreferrer" ><span class="label">ORCID</span></a></li>
      <li><a href=" {{ site.author.googlescholar }} " target="_blank"  rel="noopener noreferrer" ><span class="label">Google Scholar</span></a></li>
      <li><a href=" {{ site.author.researchgate }} " target="_blank"  rel="noopener noreferrer" ><span class="label">ResearchGate</span></a></li>
</ul>
<hr style="margin: 10px 0 10px 0;">

## Projects

{% for item in site.data.pastprojects %}
<div class="panel">
<div class="panel-heading noline" data-toggle="{{forloop.index}}">     
<img alt="" class="panel-heading-question" data-toggle="{{forloop.index}}" src="{{ 'assets/images/circle-icon.png' | absolute_url }}"><a data-toggle="{{forloop.index}}" href="javascript:void(0)">{{item.title}}</a>
</div>
<div class="panel-body hidden-element" data-body="{{forloop.index}}"> 
{{item.body}} 
</div>
</div>
{% assign projoffset = forloop.index %}
{% endfor %}

<hr> 

## Skills

<div class="grey-row">
    <div class="grey-column">
        <h1 style="color:white; font-size: 2.5em; font-weight: normal;">Astronomy</h1>
        <p  style="font-family: 'Source Sans Pro',Roboto,sans serif; color:white; margin-right: 30px; margin-top: -5px;">AIPS, CASA, SaoImage DS9.</p>
    </div>

    <div class="grey-column">
        <h1 style="color:white; font-size: 2.5em; font-weight: normal;">Languages</h1>
        <p style="font-family: 'Source Sans Pro',Roboto,sans serif; color:white; margin-right: 30px; margin-top: -5px;">C, C++, Python, FORTRAN.</p>
    </div>

    <div class="grey-column">
        <h1 style="color:white; font-size: 2.5em; font-weight: normal;">Python</h1>
        <p style="font-family: 'Source Sans Pro',Roboto,sans serif; color:white; margin-right: 30px; margin-top: -5px;">scipy, astropy, Parallel Processing.</p>
    </div>

    <div class="grey-column">
        <h1 style="color:white; font-size: 2.5em; font-weight: normal;">Text Processing</h1>
        <p style="font-family: 'Source Sans Pro',Roboto,sans serif; color:white; margin-right: 30px; margin-top: -5px;">Latex, Vim.</p>
    </div>
</div>
<hr>

## Softwares and Repositories

<div  class="posts" style="margin-bottom: 10px;">
{% for post in site.projects limit:2 %}
<article>
    <a href="{{ post.url | absolute_url }}"><p style="color: black; font-weight: 400;" >{{ post.title }}</p></a>
            <a href="{{ post.url | absolute_url }}" class="image">
                <picture>
                <source data-srcset="{{ post.image-webp | absolute_url }}" type="image/webp" >
                <source data-srcset="{{ post.image | absolute_url }}" type="image/jpeg" > 
                <img src="{{ post.image-thumb | absolute_url }}" alt="{{ post.image-alt }}" data-src="{{ post.image | absolute_url }}"  class="lazyload" />
                </picture> 
                <p style="margin-top: 10px; color: #444444;">{{ post.description }}</p>
            </a>
        </article>
  {% endfor %}
</div>
<p style="font-size: smaller;"><a href="{{ 'repos' | absolute_url }}" class="button special icon fa-code">Explore all softwares and repositories</a></p>

<hr>


## Publications
<div class="table-wrapper">
    <table>
        <tbody>
            {% for paper in site.data.publications reversed %}
            <tr>
                <h4>{{ paper.title }}</h4>
                    {% if paper.type == "conference" %}
                    <a class="tag_marker"><span>Conference Proceedings</span></a>
                    {% endif %}
                Published on : {{ paper.published }} <br>
                {{ paper.authors }} <br>
                {{ paper.journal }}<br>
                {{ paper.volumeinfo }}
                <br><br>
                <span style="display: inline;">
                    <i class="fa fa-link"></i>&nbsp;<span style="color: #333333; font-size: small;" ><b>External Links:</b></span>&nbsp;
                    {% if paper.doi %}
                      <a href="{{ paper.doi | absolute_url }}" target="_blank" rel="noopener noreferrer" class="tag_btn"><span>DOI</span></a>&nbsp;
                    {% endif %}
                    {% if paper.ads %}
                    <a href="{{ paper.ads  | absolute_url }}" target="_blank"  rel="noopener noreferrer" class="tag_btn"><span>ADS</span></a>&nbsp;
                    {% endif %}
                    {% if paper.arxiv %}
                    <a href="{{ paper.arxiv  | absolute_url }}" target="_blank" rel="noopener noreferrer"  class="tag_btn"><span>ArXiv</span></a>&nbsp;
                    {% endif %}
                </span>
            </tr>
            <hr>
            {% endfor %}
        </tbody>
    </table>
</div>
