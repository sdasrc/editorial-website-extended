---
layout: imageviewer
title: Hey! Look Ma a dying star!
image: assets/images/memes/jpg/hey-look-a-dying-star.jpg
image-webp: assets/images/memes/webp/hey-look-a-dying-star.webp
image-thumb: assets/images/memes/thumb/hey-look-a-dying-star-thumb.jpg
page-level: memepage
permalink: memes/hey-look-a-dying-star/
oc: OC
twitter-link: https://twitter.com/lordparthurnaax/status/1293030023650332673
robots: noindex
sitemap: false
---
