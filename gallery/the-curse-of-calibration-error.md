---
layout: imageviewer
title: Radio Astronomer and the Curse of the Calibration Error
image: assets/images/memes/jpg/the-curse-of-calibration-error.jpg
image-webp: assets/images/memes/webp/the-curse-of-calibration-error.webp
image-thumb: assets/images/memes/thumb/the-curse-of-calibration-error-thumb.jpg
page-level: memepage
oc: OC
permalink: memes/the-curse-of-calibration-error/
robots: noindex
sitemap: false
---
