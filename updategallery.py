import os
import re
from os import listdir
from os.path import isfile, join

# jpg  junk  lq  thumb  webp
#/home/overlord/git/soumyadeepdas.gitlab.io/assets/images/gallery/galllist.txt

basedir = 'assets/images/gallery/'
galljpgs = 'assets/images/gallery/jpg/'
gallwebp = 'assets/images/gallery/webp/'
jpgfiles = [f for f in listdir(galljpgs) if isfile(join(galljpgs, f))]
webpfiles = [g for g in listdir(gallwebp) if isfile(join(gallwebp, g))]
gallyml = '_data/gallery.yml'

outF = open(gallyml, "a")

for ii in jpgfiles:
    basename = os.path.splitext(ii)
    basename = basename[0]
    if (basename+".webp" not in webpfiles):
        os.system("convert "+galljpgs+ii+" -sampling-factor 4:2:0 -strip -quality 85   -interlace Plane -colorspace RGB "+galljpgs+ii)
        os.system("convert -resize 5% "+galljpgs+ii+" "+basedir+"thumb/"+basename+"-thumb.jpg")
        os.system("convert -resize 420x "+galljpgs+ii+" "+basedir+"420x/"+basename+"-420x.jpg")
        os.system("cwebp "+galljpgs+ii+" -o "+basedir+"webp/"+basename+".webp")
        os.system("cwebp "+basedir+"420x/"+basename+"-420x.jpg -o "+basedir+"webp-420/"+basename+"-420x.webp")
        imgalt = re.sub('[^a-zA-Z0-9\n\.]', ' ', basename)
        imgalt = imgalt.capitalize()
        outF.write('- img-src: assets/images/gallery/jpg/'+ii+'\n')
        outF.write('  img-webp: assets/images/gallery/webp/'+basename+'.webp\n')
        outF.write('  img-420x: assets/images/gallery/420x/'+basename+'-420x.jpg\n')
        outF.write('  webp-420x: assets/images/gallery/webp-420/'+basename+'-420x.webp\n')
        outF.write('  img-thumb: assets/images/gallery/thumb/'+basename+'-thumb.jpg\n')
        outF.write('  img-alt: '+imgalt+'\n')
        outF.write('  img-link: gallery/'+basename+'/\n')
        outF.write('  img-weight: 10\n')
        outF.write("\n")
        mdfile = 'gallery/'+basename+'.md'
        OpenIMF = open(mdfile, "w")
        OpenIMF.write('---\n')
        OpenIMF.write('layout: imageviewer\n')
        OpenIMF.write('title: '+imgalt+'\n')
        OpenIMF.write('image: assets/images/gallery/jpg/'+ii+'\n')
        OpenIMF.write('image-webp: assets/images/gallery/webp/'+basename+'.webp\n')
        OpenIMF.write('image-thumb: assets/images/gallery/thumb/'+basename+'-thumb.jpg\n')
        OpenIMF.write('page-level: gallpage\n')
        OpenIMF.write('permalink: gallery/'+basename+'/\n')
        OpenIMF.write('robots: noindex\n')
        OpenIMF.write('sitemap: false\n')
        OpenIMF.write('---\n')

        print("New entry created : "+basename+".")
    else:
        print("Old entry : "+basename+".")
outF.close()
