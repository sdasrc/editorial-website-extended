---
layout: page
title: Image Gallery
description: A simple gallery-eqsue implemention to display images, with on-click, save, and share functionalities.
permalink: gallery/
redirect_from:
  - img/
  - images/
---

A simple gallery-eqsue implemention to display images, with on-click, save, and share functionalities.

<a name="top"></a>
<div style="width: 90%" class="posts">
{% for post in site.data.gallery  %}
<article>
  <p>
    {% if post.oc %}
      <a style="cursor: default;font-size: small;" class="tag_marker">
        <span>{{ post.oc }}</span>
      </a>
      {% endif %}
    {% if post.source %}
      <a style="cursor: default; font-size: small;" class="tag_marker">
        <span>{{ post.source }}</span>
      </a>
      {% endif %}
    <b style="margin-bottom: 0px;">{{ post.img-alt }}</b>
    </p>
      <p style="margin-top: 0px; font-size: 4px;"> </p>
       <a href="{{ post.img-link | absolute_url }}" class="image">
        <picture style="padding-top: 10px; margin-top: 10px; ">
            <source data-srcset="{{ post.webp-420x | absolute_url }}" type="image/webp" >
            <source data-srcset="{{ post.img-420x | absolute_url }}" type="image/jpeg" > 
            <img src="{{ post.img-thumb | absolute_url }}" alt="Meme : {{ post.img-alt }}" data-src="{{ post.img | absolute_url }}"  class="lazyload" />
      </picture>    
      {% include file-socialshare.html %}           
            </a>
        </article>
  {% endfor %}
</div>

<hr>
<p style="font-size: smaller;"><a href="#top" class="button icon fa-angle-double-up">&nbsp;Back to Top</a></p>
